package encapsulation;

public class Account {

  private double balance;
  private String username;
  private double maxBalanceLimit;

  //constructor
  public Account(double balance, String username, double maxBalanceLimit) {
    this.balance = balance;
    this.username = username;
    this.maxBalanceLimit = maxBalanceLimit;
  }

  public void topup(double amount) {
    if (balance + amount < maxBalanceLimit) {
      balance = balance + amount;
      System.out.println("Topup - " + amount);
      getBalance();
    } else {
      System.err.println("Balance's getting over limit");
    }
  }

  public void withdraw(double amount) {
    if (balance - amount >= 0) {
      balance = balance - amount;
      System.out.println("Withdraw - " + amount);
      getBalance();
    }  else {
      System.err.println("Balance's not enough");
    }
  }

  public void setMaxBalanceLimit(double amount) {
    maxBalanceLimit = amount;
    System.out.println("Max is set to - " + amount);
  }

  private void engineStart() {
    System.out.println("Engine Starting");
  }

  public void getBalance() {
    System.out.println("Balance: " + balance);
  }
}
