package encapsulation;

public class Banking {

  private Account account;
  private Payment payment;

  public void makeTransaction() {
    payment.auth(account);
    payment.createReceipt();
    payment.payReceipt();
  }
}
