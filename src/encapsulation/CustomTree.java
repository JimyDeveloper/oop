package encapsulation;

import inheritance.Tree;

public class CustomTree extends Tree {

  @Override
  public void reseed() {
    System.out.println("Child seeding. Age: " + age);
  }
}
