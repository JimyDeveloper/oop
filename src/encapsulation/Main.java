package encapsulation;

public class Main {

  public static void main(String[] args) {
    Account account = new Account(100, "To'lqin", 1000);

    account.topup(1000);
    System.out.println();
    account.topup(500);
    System.out.println();
    account.withdraw(700);
    System.out.println();
    account.withdraw(300);


//    account.balance = account.balance + 1000;
//    account.getBalance();
//    System.out.println();
//    account.balance = account.balance - 7000;
//    account.getBalance();
  }
}
