package polymorphysim;

public class Trapezium extends Rectangle {


  private int upperWidth;
  private int leftHeight;

  @Override
  public void area() {
    int area = width + height + upperWidth + leftHeight;
    System.out.println("Area of Trapezium is " + area) ;
  }
}
