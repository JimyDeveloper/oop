package polymorphysim;

public class Rectangle {

  protected int width;
  protected int height;

  public void area() {
    int area = 2 * (width + height);
    System.out.println("Area of rectangle is " + area) ;
  }
}
