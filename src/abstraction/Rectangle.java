package abstraction;

public class Rectangle extends Shape {

  public Rectangle(int width, int height, String color) {
    super(color);
    this.width = width;
    this.height = height;
  }

  private int width;
  private int height;

  @Override
  public void getArea() {
    int area = 2 * (width + height);
    System.out.println("Rectangle Area is " + area);
  }

  @Override
  public void asText() {
    System.out.println("Rectangle with width " + width + " and height " + height);
  }
}
