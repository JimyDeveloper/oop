package abstraction;

import encapsulation.Account;

public class Main {

  public static void main(String[] args) {
    Shape circle = new Circle(5, "Red");
    circle.getArea();
    circle.asText();
    circle.getColor();

    System.out.println();
    System.out.println();


    Shape rectangle = new Rectangle(4, 5, "Blue");
    rectangle.getArea();
    rectangle.asText();
    rectangle.getColor();


    Account account = new Account(10, "Jimy", 500);
    account.getBalance();
  }
}
