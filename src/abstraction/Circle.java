package abstraction;

public class Circle extends Shape {

  public Circle(double radius, String color) {
    super(color);
    this.radius = radius;
  }

  private double radius;

  @Override
  public void getArea() {
    double area = 2 * 3.14 * radius;
    System.out.println("Circle Area is " + area);
  }

  @Override
  public void asText() {
    System.out.println("Circle with radius " + radius);
  }
}
