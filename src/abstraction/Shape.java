package abstraction;

public abstract class Shape {


  public Shape(String color) {
    this.color = color;
  }

  private String color;

  public abstract void getArea();

  public abstract void asText();

  public void getColor() {
    System.out.println("Color " + color);
  }
}
