package inheritance;

public class PalmTree extends Tree {

  @Override
  public void reseed() {
    System.out.println("Child seeding. Age: " + age);
  }
}
