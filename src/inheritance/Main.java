package inheritance;

public class Main {

  public static void main(String[] args) {
    Tree palmTree = new PalmTree();

    palmTree.reseed();
    System.out.println();
    palmTree.grow();
  }
}
