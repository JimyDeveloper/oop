package inheritance;

public class Tree {

  private String species;
  private double height;
  protected int age;

  public void grow() {
    System.out.println("Parent: Growing");
  }

  public void reseed() {
    System.out.println("Parent: Reseeding");
  }
}
